# Curso de programação em AWK

Repositório de recursos do curso de programação em AWK. Estas apostilas ainda não foram devidamente trabalhadas e devem ser vistas, por enquanto, como o roteiro das aulas que foram dadas ao vivo pelo Youtube. Mesmo assim, trata-se de um material confiável para todos que quiserem começar a programar em AWK.

![](https://blauaraujo.com/wp-content/uploads/2022/05/cafezinho-01.png)

## Links importantes

* [Índice das aulas](aulas/README.md)
* [Playlist completa no Youtube](https://youtube.com/playlist?list=PLXoSGejyuQGr_ZbyClFpnz8K3Or2jK9cG)
* [Vídeos e textos para aprender o shell do GNU/Linux](https://codeberg.org/blau_araujo/para-aprender-shell)
* [Dúvidas sobre os tópicos do curso (issues)](https://codeberg.org/blau_araujo/curso-awk/issues)
* [Discussões sobre os tópicos de todos os nossos cursos (Gitter)](https://gitter.im/blau_araujo/community)


## Formas de apoio

* [Apoio mensal pelo Apoia.se](https://apoia.se/debxpcursos)
* [Doações pelo PicPay](https://app.picpay.com/user/blauaraujo)
* Doações via PIX: pix@blauaraujo.com
* [Versão impressa do Pequeno Manual do Programador GNU/Bash](https://blauaraujo.com/2022/02/17/versao-impressa-do-pequeno-manual-do-programador-gnu-bash/)