# Aula 9 - Entradas e saídas

- [Vídeo desta aula](https://youtu.be/E2jDJCZ203w)

## 9.1 - Escrevendo a saída em um arquivo

* O AWK permite a escrita em arquivos da mesma forma que o shell faria: com redirecionamentos do tipo `>` e `>>`.

* A diferença é que esses redirecionamentos podem acontecer dentro do próprio programa:

```
awk 'REGRA { ...; print > (ou) >> arquivo-destino }' arquivos-dados
```

#### Exemplo (com o arquivo 'brazilian-cities.csv')

```
awk 'BEGIN { FS="," } { print >> "cidades-"$1".csv"; }' brazilian-cities.csv
```

## 9.2 - Enviando a saída para outro programa

* Além dos redirecionamentos para arquivos, também podemos enviar saídas do AWK para a entrada de outros programas com um ***pipe***.

#### Exemplo:

```
awk '{ print | "grep RN" }' brazilian-cities.csv
```

## 9.3 - O builtin 'getline'

* É um tópico avançado, mas tem alguns usos práticos bem simples:

### Exemplo: Obter o próximo registro

```
awk '$0 ~ "Serrinha" { print; getline; print; }' brazilian-cities.csv
```

Saída:

```
BA,Serrinha,76762,624.23
BA,Serrolândia,12344,295.85
RN,Serrinha,6581,193.35
RN,Serrinha dos Pintos,4540,122.65 ← Observe que este registro
                                     foi dado como lido e não
                                     passou pela regra!
```

Note que isso é diferente de um outro builtin, o `next`, que força o processamento imediato da linha seguinte do arquivo:

```
awk '$0 ~ "Serrinha" { print; next; print; }' brazilian-cities.csv
```

Saída:

```
BA,Serrinha,76762,624.23
RN,Serrinha,6581,193.35
RN,Serrinha dos Pintos,4540,122.65
```

### Exemplo: Lendo todo o arquivo

```
awk 'BEGIN { while (getline < "cidades-AL.csv") print }'
```

Isso é muito útil quando queremos fazer programas que processem arquivos pré-definidos:

```
#!/usr/bin/env -S awk -f

BEGIN {
    data = "./brazilian-cities.csv"

    while( (getline linha < data) > 0 ) {
        print linha
    }

}
```

### Exemplo: Fim do arquivo

```
awk '$0 ~ "Xambioá" { print; getline; print; }' brazilian-cities.csv
```

Saída:

```
TO,Xambioá,11484,1186.43
TO,Xambioá,11484,1186.43
```

> O builtin `getline` retorna `1` se encontrar um registro ou `0` se encontrar EOF.

```
awk '$0 ~ "Xambioá" { print; if (getline > 0) print; }' brazilian-cities.csv
```

Saída:

```
TO,Xambioá,11484,1186.43
```

### Exemplo: Lendo entradas do utilizador

```
awk 'BEGIN { print "Digite o seu nome: "; getline < "-"; print  "Seu nome é " $0; }'
```

Executando:

```
Digite o seu nome:
Blau Araujo
Seu nome é Blau Araujo
```

Como funciona:

*  Com o redirecionamento da entrada, o `getline` leria um arquivo.
* A entrada padrão (stdin) também é um arquivo, e ela é representada em qualquer versão do awk como um traço (`-`).
* No 'gawk' nós podemos utilizar `/dev/stdin` no lugar do traço.

### Outra forma de ler a entrada padrão com o traço

```
cat cidades-RN.csv | awk '$0 ~ "Serrinha" { print }' cidades-BA.csv -
```
