# Aula 1 - Conhecendo o AWK

- [Vídeo desta aula](https://youtu.be/ISeFxCm7MDM)

## 1.1 - O que é o AWK

O AWK é um utilitário que interpreta uma linguagem de programação especializada na manipulação e formatação de dados estruturados em texto. O nome AWK vem das iniciais de seus desenvolvedores:

* Alfred V. Aho
* Peter J. Weinberger
* Brian W. Kerningan

## 1.2 - A implementação GNU do AWK

Em 1986, Paul Rubin escreveu a primeira implementação do Projeto GNU, o ***gawk***. No anos seguintes, o gawk sofreu vários aprimoramentos, até ser completamente retrabalhado, em 1994, para tornar-se compatível com as evoluções do AWK Unix.

> Existem várias outras implementações do AWK, e algumas delas têm sido adotadas como padrão em alguns sistemas Unix like, mas o foco deste curso será a implementação do Projeto GNU.

## 1.3 - O que podemos fazer com o AWK

Com o AWK, nós podemos...

* Gerenciar pequenas bases de dados
* Gerar relatórios
* Validar dados
* Indexar e processar documentos
* Testar algorítimos

### Funcionalidades adicionais da implementação GNU

Com o GAWK, nós podemos...

* Extrair partes específicas dos dados para processamento
* Classificar os dados (sort)
* Abrir conexões TCP/IP para processar dados vindos da rede
* Analisar programas em AWK
* Estender a linguagem com funções escritas em C ou C++

## 1.4 - Uma linguagem orientada a dados

A linguagem AWK pode ser descrita como uma **linguagem interpretada orientada a dados**.

### O que isso significa?

Na maioria das outras linguagens, o nosso trabalho é descrever em detalhes as rotinas e os procedimentos, o que torna mais difícil descrever os dados com que o programa irá processar.

> Na linguagem AWK, nós descrevemos os dados com que queremos trabalhar para, num segundo momento, dizermos o que fazer com esses dados quando eles forem encontrados.

A própria forma de execução do AWK (o interpretador) reflete essa característica da linguagem. Ao ser chamado, o AWK espera encontrar o programa que dirá a ele o que fazer com os dados.

O programa em si consiste numa série de regras, cada uma delas especificando um padrão de busca e as ações que deverão ser executadas caso o padrão tenha sido encontrado.

## 1.5 - Checando a sua versão do AWK

O mais comum é que as distribuições GNU/Linux venham com a implementação GNU do AWK (gawk), mas pode ser que outras implementações estejam instaladas e configuradas como padrão. Uma forma simples de verificar o seu caso é executando...

```
:~$ awk -V
```

Se a implementação for outra, é possível que este comando produza um erro na saída e será necessário instalar o gawk de acordo com os procedimentos da sua distribuição.

> Mesmo que não aconteça um erro, você saberá qual implementação está instalada no seu sistema.

## 1.6 - O conceito de regras

Uma regra consiste de um padrão seguido de uma ação. A ação é envolvida por chaves ({ ... }) para separá-la do padrão, e as regras geralmente são separadas entre si por uma quebra de linha.

```
PADRÃO { AÇÕES }
PADRÃO { AÇÕES }
PADRÃO { AÇÕES }
...
```

Os padrões podem ser:

* Expressões
* Expressões regulares
* Faixas de registros (linhas, por padrão)
* Regras de pré e pós-processamento (BEGIN/END)
* Padrões para controle avançado de arquivos (BEGINFILE/ENDFILE)
* Vazio (casa com todos os registros)

## 1.7 - Como executar programas em AWK

Como vimos no início desta aula, existem três formas de executar um programa em AWK. Mas, desta vez, vamos nos concentrar um pouco mais na possibilidade de criarmos executáveis em AWK.

### A linha do interpretador

Geralmente, basta escrever a shebang assim:

```
#!/bin/awk -f
```

Contudo, nem todos os sistemas instalam (ou linkam) o AWK na pasta `/bin`. Alternativamente, podemos recorrer ao utilitário `env` para que ele localize e execute o interpretador da linguagem AWK:

```
#!/usr/bin/env -S awk -f
```

Neste caso, a opção -S (*split string*) fará com que `awk -f` seja entendido como um comando e um argumento.

### Tornando o programa executável

Outro requisito para que um arquivo contendo um código em AWK seja executado diretamente na linha de comando é a permissão para execução, o que pode ser feito desta forma:

```
:~$ chmod +x arquivo-do-programa
```

> Para saber mais sobre permissões de arquivos, é altamente recomendável assistir a [esta aula](https://www.youtube.com/watch?v=CeeNPDMUntA) do Professor Paulo Kretcheu.

### O caminho do programa

Sempre que chamamos um executável pela linha de comando, nós precisamos indicar onde ele está localizado informando o seu caminho.

```
:~$ /caminho/arquivo-do-programa
```

Alguns caminhos, porém, já estão definidos na variável de ambiente `PATH`, e o shell tentará localizar o programa nesses locais caso nenhum caminho seja informado.

Em todos os outros casos, mesmo que o programa esteja na mesma pasta onde estamos dando o comando para a sua execução, nós somos obrigados a informar a sua localização.

Se o programa estiver na pasta de trabalho atual, este caminho recebe a identificação especial representada por um ponto (`.`). Portanto, o caminho pode ser indicado desta forma:

```
:~$ ./arquivo-do-programa
```

### Aspas

Os programas em AWK só são escritos entre aspas simples em duas situações:

* Na linha de comando
* Em scripts em shell

Isso é necessário apenas para que o shell identifique o programa como um argumento do comando `awk`. Mas, se o programa estiver em um arquivo (executável ou não) as aspas não são utilizadas nem na linha de comando, nem internamente no arquivo.

## 1.8 - Executando o AWK sem arquivos de dados

Também é possível executar o AWK sem arquivos de entrada. Observe o exemplo abaixo:

```
awk '{ input = input $0" " }; END {print input}'
```

Aqui, o AWK entrará no modo de "escuta" da entrada padrão, e cada linha digitada na entrada padrão será entendida como um registro de dados. A leitura da entrada padrão termina quando teclamos `{Ctrl}`+`{D}`.

> Repare que utilizamos duas regras neste exemplo: a primeira lê todas as linhas digitadas e processa as ações entre as chaves (no caso, uma concatenação de strings), enquanto a segunda, definida pelo padrão `END`, só terá suas ações executadas quando a leitura do arquivo (entrada padrão, no nosso caso) terminar.

Outra forma de executar o AWK sem arquivos de entrada é com o padrão `BEGIN`, e isso nos dá uma boa oportunidade para o tradicional "Olá, mundo!":

```
:~$ awk 'BEGIN { print "Olá, mundo!" }'
Olá, mundo!
```

O padrão `BEGIN` faz com que as ações associadas a ele sejam executadas antes da leitura de qualquer arquivo. Como não há regras a serem processadas depois dele, o programa é encerrado.

## 1.9 - Nosso primeiro programa em AWK

***Acompanhe no vídeo!***

Neste primeiro programa, nós faremos vários tipos de leituras e filtragens de dados no arquivo `catalogo.csv`:

```
Nome,Idade,Rua,Número,Cidade,UF
João da Silva,23,Av. das Couves,123,São Paulo,SP
Antônio Grilo,32,Rua da Feira,33,Sorocaba,SP
Marcus Pereira,28,Rua da Várzea,42,Macaé,RJ
Felipa dos Santos,20,Av. Brasil,147,Salvador,BA
Maria Antonieta,45,Av. das Flores,1234,Osasco,SP
Sofia Boole,19,Rua das Camélias,768,Itatiaia,RJ
Adriano Campos,27,Rua Direita,98,Manaus,AM
José Andrade,42,Av. do Quilombo,765,Olinda,PE
Sérgio Patinhas,33,Av. Disney,54,Patópolis,MG
```

Nosso principal objetivo hoje é compreender o funcionamento das regras e padrões, mas utilizaremos alguns outros elementos da linguagem AWK que serão melhor estudados nas próximas aulas.

```
# Exibindo todo o arquivo...
:~$ awk '{ print }' catalogo.csv

# Ou ainda...
:~$ awk '1' catalogo.csv

# Exibindo apenas a primeira linha...
:~$ awk 'NR==1 { print }' catalogo.csv

# Exibindo da segunda à quinta linha...
:~$ awk 'NR==2,NR==5  { print }' catalogo.csv

# Exibindo todas as linhas menos a primeira...
:~$ awk 'NR!=1  { print }' catalogo.csv

# Exibindo o primeiro campo (coluna) de todos os registros (linhas)...
:~$ awk '{ print $1 }' catalogo.csv

# Importante! Repare que o AWK identificou o primeiro campo
# considerando o separador padrão: espaços!

# Exibindo o primeiro campo com definição de separador...
:~$ awk -F',' '{ print $1 }' catalogo.csv

# Exibindo registros onde a idade está entre 20 e 29 anos...
:~$ awk -F',' '$2 < 30 && $2 >= 20 { print }' catalogo.csv

# Exibindo registros de moradores de avenidas...
:~$ awk -F',' '$3 ~ "Av" { print }' catalogo.csv

# Exibindo moradores do estado do Rio de Janeiro...
:~$ awk -F',' '$NF=="RJ" { print }' catalogo.csv

# Exibindo nomes que termninam em 'ia'...
:~$ awk -F',' '/^.*ia .*$/ { print }' catalogo.csv
```