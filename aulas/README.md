# Índice

## [1 - Conhecendo o AWK](aula-01.md)

[1.1 - O que é o AWK](aula-01.md#1-1-o-que-%C3%A9-o-awk)

[1.2 - 1.2 - A implementação GNU do AWK](aula-01.md#1-2-a-implementa%C3%A7%C3%A3o-gnu-do-awk)

[1.3 - O que podemos fazer com o AWK](aula-01.md#1-3-o-que-podemos-fazer-com-o-awk)

- [Funcionalidades adicionais da implementação GNU](aula-01.md#funcionalidades-adicionais-da-implementa%C3%A7%C3%A3o-gnu)

[1.4 - Uma linguagem orientada a dados](aula-01.md#1-4-uma-linguagem-orientada-a-dados)

- [O que isso significa?](aula-01.md#o-que-isso-significa)

[1.5 - Checando a sua versão do AWK](aula-01.md#1-5-checando-a-sua-vers%C3%A3o-do-awk)

[1.6 - O conceito de regras](aula-01.md#1-6-o-conceito-de-regras)

[1.7 - Como executar programas em AWK](aula-01.md#1-7-como-executar-programas-em-awk)

- [A linha do interpretador](aula-01.md#a-linha-do-interpretador)
- [Tornando o programa executável](aula-01.md#tornando-o-programa-execut%C3%A1vel)
- [O caminho do programa](aula-01.md#o-caminho-do-programa)
- [Aspas](aula-01.md#aspas)

[1.8 - Executando o AWK sem arquivos de dados](aula-01.md#1-8-executando-o-awk-sem-arquivos-de-dados)

[1.9 - Nosso primeiro programa em AWK](aula-01.md#1-9-nosso-primeiro-programa-em-awk)

## [2. Separação de campos](aula-02.md)

[2.1 - Especificando a separação de campos](aula-02.md#2-1-especificando-a-separa%C3%A7%C3%A3o-de-campos)

[2.2 - A separação padrão e a variável FS](aula-02.md#2-2-a-separa%C3%A7%C3%A3o-padr%C3%A3o-e-a-vari%C3%A1vel-fs)

[2.3 - Exemplos](aula-02.md#2-3-exemplos)

- [Exemplo 1: separação padrão](aula-02.md#exemplo-1-separa%C3%A7%C3%A3o-padr%C3%A3o)
- [Exemplo 2: separando pelas vírgulas](aula-02.md#exemplo-2-separando-pelas-v%C3%ADrgulas)
- [Exemplo 3: uma possível solução](aula-02.md#exemplo-3-uma-poss%C3%ADvel-solu%C3%A7%C3%A3o)

[2.4 - Separando campos com expressões regulares](aula-02.md#2-4-separando-campos-com-express%C3%B5es-regulares)

[2.5 - Tornando cada caractere um campo](aula-02.md#2-5-tornando-cada-caractere-um-campo)

[2.6 - Campos de linha inteira](aula-02.md#2-6-campos-de-linha-inteira)

[2.7 - Lendo dados de largura fixa](aula-02.md#2-7-lendo-dados-de-largura-fixa)

[2.8 - Definindo campos pelo conteúdo](aula-02.md#2-8-definindo-campos-pelo-conte%C3%BAdo)

- [A variável 'FPAT'!](aula-02.md#a-vari%C3%A1vel-fpat)

[2.9 - Registros com múltiplas linhas](aula-02.md#2-9-registros-com-m%C3%BAltiplas-linhas)

## [3. Prática: lendo um feed RSS](aula-03.md)

Apenas [em vídeo](https://youtu.be/1QZspNDOJQM).

## [4. Controlando a saída de dados](aula-04)

[4.1 - A instrução 'print'](aula-04.md#4-1-a-instru%C3%A7%C3%A3o-print)

[4.2 - Separadores de saída](aula-04.md#4-2-separadores-de-sa%C3%ADda)

- [Separador de campos de saída (OFS)](aula-04.md#separador-de-campos-de-sa%C3%ADda-ofs)
- [Separador de registros de saída (ORS)](aula-04.md#separador-de-registros-de-sa%C3%ADda-ors)

[4.3 - Formatando saídas numéricas](aula-04.md#4-3-formatando-sa%C3%ADdas-num%C3%A9ricas)

- [Especificando formatos de saída (OFMT)](aula-04.md#especificando-formatos-de-sa%C3%ADda-ofmt)

[4.4 - Formatando a saída com a instrução 'printf'](aula-04.md#4-4-formatando-a-sa%C3%ADda-com-a-instru%C3%A7%C3%A3o-printf)

[4.5 - Redirecionando a saída para arquivos](aula-04.md#4-5-redirecionando-a-sa%C3%ADda-para-arquivos)

- [Redirecionando para um novo arquivo](aula-04.md#redirecionando-para-um-novo-arquivo)
- [Redirecionando para o final de um arquivo (append)](aula-04.md#redirecionando-para-o-final-de-um-arquivo-append)

[4.6 - Redirecionando a saída para comandos](aula-04.md#4-6-redirecionando-a-sa%C3%ADda-para-comandos)

## [5. Variáveis e Arrays](aula-05.md)

[5.1 - Variáveis no AWK](aula-05.md#5-1-vari%C3%A1veis-no-awk)

[5.2 - Definindo variáveis na linha de comando](aula-05.md#5-2-definindo-vari%C3%A1veis-na-linha-de-comando)

- [Criando variáveis com a opção '-v'](aula-05.md#criando-vari%C3%A1veis-com-a-op%C3%A7%C3%A3o-v)
- [Criando variáveis entre os nomes de arquivos](aula-05.md#criando-vari%C3%A1veis-entre-os-nomes-de-arquivos)

[5.3 - Como o AWK converte strings e números](aula-05.md#5-3-como-o-awk-converte-strings-e-n%C3%BAmeros)

[5.4 - Trabalhando com arrays](aula-05.md#5-4-trabalhando-com-arrays)

[5.5 - Percorrendo os valores em uma array](aula-05.md#5-5-percorrendo-os-valores-em-uma-array)

[5.6 - Utilizando números como índice](aula-05.md#5-6-utilizando-n%C3%BAmeros-como-%C3%ADndice)

[5.7 - Lidando com variáveis indefinidas como índice](aula-05.md#5-7-lidando-com-vari%C3%A1veis-indefinidas-como-%C3%ADndice)

[5.8 - A instrução 'delete'](aula-05.md#5-8-a-instru%C3%A7%C3%A3o-delete)

## [6. Arrays multidimensionais](aula-06.md)

[6.1 - O que é uma array multidimensional](aula-06.md#6-1-o-que-%C3%A9-uma-array-multidimensional)

[6.2 - Como fica o operador 'in'](aula-06.md#6-2-como-fica-o-operador-in)

[6.3 - Percorrendo os elementos de uma array multidimensional](aula-06.md#6-3-percorrendo-os-elementos-de-uma-array-multidimensional)

[6.4 - Trabalhando com arrays "realmente" multidimensionais (gawk)](aula-06.md#6-4-trabalhando-com-arrays-realmente-multidimensionais-gawk)

## [7. Funções internas](aula-07.md)

[7.1 - O conceito de 'função'](aula-07.md#7-1-o-conceito-de-fun%C3%A7%C3%A3o)

[7.2 - Chamando funções no AWK](aula-07.md#7-2-chamando-fun%C3%A7%C3%B5es-no-awk)

[7.3 - Funções numéricas](aula-07.md#7-3-fun%C3%A7%C3%B5es-num%C3%A9ricas)

[7.4 - Funções de strings](aula-07.md#7-4-fun%C3%A7%C3%B5es-de-strings)

- [Função 'index'](aula-07.md#fun%C3%A7%C3%A3o-index)
- [Função 'length'](aula-07.md#fun%C3%A7%C3%A3o-length)
- [Função 'match'](aula-07.md#fun%C3%A7%C3%A3o-match)
- [Função 'substr'](aula-07.md#fun%C3%A7%C3%A3o-substr)
- [Função 'split'](aula-07.md#fun%C3%A7%C3%A3o-split)
- [Função 'sprintf'](aula-07.md#fun%C3%A7%C3%A3o-sprintf)
- [Função 'printf'](aula-07.md#fun%C3%A7%C3%A3o-printf)
- [Função 'sub'](aula-07.md#fun%C3%A7%C3%A3o-sub)
- [Função 'gsub'](aula-07.md#fun%C3%A7%C3%A3o-gsub)
- [Função 'gensub'](aula-07.md#fun%C3%A7%C3%A3o-gensub)
- [Funções 'tolower' e 'toupper'](aula-07.md#fun%C3%A7%C3%B5es-tolower-e-toupper)

[7.5 - Funções de entrada e saída](aula-07.md#7-5-fun%C3%A7%C3%B5es-de-entrada-e-sa%C3%ADda)

- [Função 'close'](aula-07.md#fun%C3%A7%C3%A3o-close)
- [Função 'fflush'](aula-07.md#fun%C3%A7%C3%A3o-fflush)
- [Função 'system'](aula-07.md#fun%C3%A7%C3%A3o-system)

[7.6 - Funções de data e hora](aula-07.md#7-6-fun%C3%A7%C3%B5es-de-data-e-hora)

- [Função 'systime'](aula-07.md#fun%C3%A7%C3%A3o-systime)
- [Função 'strftime'](aula-07.md#fun%C3%A7%C3%A3o-strftime)

## [8. Prática e revisão](aula-08.md)

[Exercício 1](aula-08.md#exerc%C3%ADcio-1)

[Exercício 2](aula-08.md#exerc%C3%ADcio-2)

[Execício 3](aula-08.md#exec%C3%ADcio-3)

[Exercício 4](aula-08.md#exerc%C3%ADcio-4)

[Arquivos utilizados](aula-08.md#arquivos-utilizados)

- [funcionarios.data](aula-08.md#funcionarios-data)
- [produtos.data](aula-08.md#produtos-data)

## [9. Entradas e saídas](aula-09)

[9.1 - Escrevendo a saída em um arquivo](aula-09.md#9-1-escrevendo-a-sa%C3%ADda-em-um-arquivo)

[9.2 - Enviando a saída para outro programa](aula-09.md#9-2-enviando-a-sa%C3%ADda-para-outro-programa)

[9.3 - O builtin 'getline'](aula-09.md#9-3-o-builtin-getline)

- [Exemplo: Obter o próximo registro](aula-09.md#exemplo-obter-o-pr%C3%B3ximo-registro)
- [Exemplo: Lendo todo o arquivo](aula-09.md#exemplo-lendo-todo-o-arquivo)
- [Exemplo: Fim do arquivo](aula-09.md#exemplo-fim-do-arquivo)
- [Exemplo: Lendo entradas do utilizador](aula-09.md#exemplo-lendo-entradas-do-utilizador)
- [Outra forma de ler a entrada padrão com o traço](aula-09.md#outra-forma-de-ler-a-entrada-padr%C3%A3o-com-o-tra%C3%A7o)

