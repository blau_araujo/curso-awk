# Aula 7 - Funções internas

- [Vídeo desta aula](https://youtu.be/uUM2uKRl_rk)

## 7.1 - O conceito de 'função'

* De modo geral, uma função é um conjunto de instruções agrupadas para realizar uma tarefa específica.
* Toda função interrompe a execução do fluxo principal do programa, executa sua própria rotina, e devolve o controle da execução para o fluxo principal.
* As funções podem receber parâmetros de execução através de argumentos passados na sua chamada.
* Algumas funções retornam valores, enquanto outras apenas realizam alguma tarefa ou alteram valores.
* Quando a função retorna um valor, ela pode ser utilizada para "expressar" este valor.
* Funções internas (ou "builtin"), são aquelas que estão sempre disponíveis para serem chamadas a partir de um programa em AWK.
* O AWK também permite que o utilizador crie suas próprias funções, mas estas só estariam disponíveis se definidas no corpo do próprio programa.

## 7.2 - Chamando funções no AWK

Funções são chamadas pelo seu nome seguido de argumentos (se houver) entre parêntesis:

```
func(arg1, arg2,...)
```

* Na chamada de funções internas, espaços entre o nome e os parêntesis são ignorados, mas isso deve ser evitado.
* Na chamada de funções definidas pelo utilizador, esses espaços são proibidos.
* A quantidade de argumentos é determinado na própria definição da função.
* Algumas funções possuem parâmetros com valores assumidos por padrão, o que permitiria a omissão dos argumentos correspondentes na chamada.
* O que não podemos fazer é passar mais argumentos do que a função espera fazer: dependendo da implementação do AWK, eles serão ignorados ou causarão um erro (GAWK).
* Expressões passadas como argumentos são avaliadas antes da execução da função, mas não podemos contar com uma ordem específica de avaliação dessas expressões.

#### Exemplo:

```
i = 5
func(i++, i *= 2)
```

Avaliação da esquerda para a direita:

```
func(6, 12)
```

Avaliação da direita para a esquerda:

```
func(11, 10)
```

## 7.3 - Funções numéricas

| Função | Desacrição |
|---|---|
| `int(x)` | Trunca um valor numérico para a sua parte inteira. |
| `sqrt(x)` | Retorna a raiz quadrada de `x`. |
| `exp(x)` | Retorna o exponencial de `x` (`e^x`). |
| `log(x)` | Retorna o logarítimo natural de `x`. |
| `sin(x)` | Retorna o seno de `x` em radianos. |
| `cos(x)` | Retorna o cosseno de `x` em radianos. |
| `atan2(y,x)` | Retorna a tangente inversa em radianos de um ponto na circunferência definido pelas coordenadas nos eixos `y` e `x`. Exemplo: `pi = atan2(0, -1)` |
| `rand()` | Retorna um número aleatório entre `0` e `1`, mas nunca igual a `0` ou `1`. |
| `srand([x])` | Semeia um valor inicial (*seed*) para a geração de números aleatórios com a função `rand()`. Por exemplo, um dado de 6 lados: `srand(); print int(rand() * 6) + 1` |


## 7.4 - Funções de strings

### Função 'index'

```
index(CONTEXTO, BUSCA)
```

Retorna a posição da primeira ocorrência da string BUSCA na string CONTEXTO, ou zero se nada for encontrado.

#### Exemplos:

```
index("laranja", "an")   → 4
index("laranja", "l")    → 1
index("laranja", "a")    → 2
index("laranja", "nada") → 0
```

### Função 'length'

```
length([STRING])
```

Retorna o número de caracteres da STRING. Sem argumento, retorna a quantidade de caracteres do registro (`$0`).

#### Exemplos:

```
length("laranja") → 7
length("pera")    → 4
length("maçã")    → 4
length(65535)     → 5
```

### Função 'match'

```
match(STRING, REGEX)
```

Procura em STRING a correspondência mais longa e mais a esquerda ao padrão definido em REGEX e retorna a posição em caracteres (índice) de onde a correspondência começa, ou zero se nada for encontrado. A função também define as variáveis `RSTART`, com o valor do índice, e `RLENGTH`, com o comprimento da substring encontrada.

#### Exemplo:

```
#               1         2
#      12345678901234567890123
str = "pera, uva, salada mista"
regex = "a.a"

print match(str, regex) → 13

regex = "a.*a"

print match(str, regex) → 4

print RSTART, RLENGTH → 4 20
```

### Função 'substr'

```
substr(STRING, INÍCIO, [COMPRIMENTO])
```

Retorna o trecho de STRING que começa no índice INÍCIO e tem a quantidade de caracteres definida em COMPRIMENTO. Se o COMPRIMENTO for omitido, a função retorna todo o restante da STRING a partir do INÍCIO.

#### Exemplo:

```
#               1         2
#      12345678901234567890123
str = "pera, uva, salada mista"
busca = "uva"
i = index(str, busca) -----------------→ 7
print substr(str, i, length(busca)) ---→ 'uva'

busca="s.* "
match(str,busca)
print substr(str, RSTART, RLENGTH) ----→ 'salada '
print substr(str, RSTART) -------------→ 'salada mista'
```

### Função 'split'

```
split(STRING, ARRAY, [SEPARADOR])**
```

Divide STRING em porções definidas pelo SEPARADOR e armazena as partes em uma ARRAY. Se um separador não for informado, será utilizado o valor em `FS`.

### Função 'sprintf'

```
sprintf(FORMATO, EXPRESSÕES)
```

Retorna uma string conforme as definições na string de FORMATO interpolando os valores das expressões.

### Função 'printf'

```
printf(FORMATO, EXPRESSÕES)
```

Imprime uma string conforme as definições na string de FORMATO interpolando os valores das expressões.

### Função 'sub'

```
sub(REGEX, SUBSTITUIÇÃO, [ALVO])
```

Altera o valor de ALVO substituindo a correspondência mais longa e mais à esquerda com o padrão definido em REGEX pela string SUBSTITUIÇÃO. Se o ALVO for omitido, será utilizada a string do registro atual (`$0`).

#### Exemplo:

```
str = "banana cana bandana"
sub(/na/, "ca", str)
print str -----------------→ bacana cana bandana
```

> **Importante!** O caractere `&` tem valor especial e representa a inserção da exata correspondência encontrada na SUBSTITUIÇÃO.

#### Exemplo:

```
str = "banana cana bandana"
sub(/cana/, "\"&\"", str)
print str -----------------→ banana "cana" bandana
```

### Função 'gsub'

```
gsub(REGEX, SUBSTITUIÇÃO, [ALVO])
```

Semelhante à função `sub()`, mas substitui todas as correspondências encontradas. Retorna o número de substituições feitas.

### Função 'gensub'

```
gensub(REGEX, SUBSTITUIÇÃO, COMO, [ALVO])
```

Se parece com `sub()` e `gsub()`, mas inclui o parâmetro COMO para especificar se todas ("g" ou "G", de "global") ou se apenas a correspondência de número 'n' serão substituídas. Outra diferença é que `gensub()` retorna a string modificada.

### Funções 'tolower' e 'toupper'

```
tolower(STRING)
toupper(STRING)
```

Retorna uma cópia da STRING em caixa alta (`toupper`) ou em caixa baixa (`tolower`).

## 7.5 - Funções de entrada e saída

### Função 'close'

```
close(ARQUIVO|COMANDO)
```

Fecha ARQUIVO ou COMANDO para leitura ou escrita.

### Função 'fflush'

```
fflush([ARQUIVO])
```

Descarrega qualquer saída em "buffer" associada ao ARQUIVO.

### Função 'system'

```
system(COMANDO)
```

Executa um COMANDO do sistema operacional.

## 7.6 - Funções de data e hora

### Função 'systime'

```
systime()
```

Retorna o número de segundos desde o início da era do sistema operacional. Em sistemas POSIX, o ínício é a meia-noite de primeiro de janeiro de 1970, UTC (era Unix).

### Função 'strftime'

```
strftime([FORMATO [, TIMESTAMP]])
```

Retorna a string TIMESTAMP formatada segundo as definições na string FORMATO. Se a TIMESTAMP não for informada, o gawk utilizará a data e a hora atuais. Se FORMATO não for informado, a função utilizará o padrão `"%a %b %d %H:%M:%S %Z %Y"`.
