# Aula 8 - Prática e revisão

- [Vídeo desta aula](https://youtu.be/cCXJlfVGEjQ)

## Exercício 1

Com o arquivo [produtos.data](#produtos-data), produza as saídas abaixo no terminal:

### Saída 1

```
:~$ awk ??? produtos.data
Produtos nacionais      : (total)
Produtos importados     : (total)
Procedência desconhecida: (total)
```

### Saída 2

```
:~$ awk ??? produtos.data
Produtos nacionais:
???
???
...

Produtos importados:
???
???
...

Procedência desconhecida:
???
???
...
```

## Exercício 2

Ainda com o arquivo de dados anterior, crie um programa em AWK que permita exibir os nomes dos produtos que contenham um dado termo passado por uma variável externa.

Por exemplo:

```
:~$ awk ??? -v busca='giz' ??? produtos.data
Giz Pastel Seco
Giz Pastel Óleo
Giz Carvão
```

## Execício 3

Utilize o AWK para analisar o arquivo [funcionarios.data](#funcionarios-data) e responder:

* Quantos **estágiários** existem na empresa;
* Quant`[ao]`s **secretári`[ao]`s** existem na empresa;
* Quem é o funcionário mais velho na folha de pagamento;
* Quem é o funcionário mais novo na folha de pagamento;
* Quem recebe o menor salário pago;
* Quem recebe o maior salário pago;

## Exercício 4

Exiba o conteúdo de [funcionarios.data](#funcionarios-data) na forma de uma tabela ordenada por função.

## Arquivos utilizados

#### funcionarios.data

```
PEDRO PEDREIRO
27
MECÂNICO
1890.00

MARÍLIA MACEDO
34
SECRETÁRIA
2345.00

ESPEDITO ESTEVÃO
19
ESTAGIÁRIO
789.00

JUCA BALA
18
ESTAGIÁRIO
789.00

JOÃO LEITÃO
29
ELETRICISTA
1720.00

MARCOS SCOMAR
33
SECRETÁRIO
2345.00
```

#### produtos.data

```
Produto            Nacional Importado
-------------------------------------------
Prego              x
Parafuso           x
Grampeador                  x
Cola Acrílica               x
Fita Adesiva                x
Bloco A4 90g       x
Bloco Canson A3    x
Giz Pastel Seco             x
Tinta Acrílica     x
Tinta Óleo                  x
Giz Pastel Óleo
Giz Carvão                  x
Esfuminho          x
Tesoura Pequena    x
Estilete                    x
Cavalete Médio     x
Prancheta A2                x
Luminária          x
EVA 5mm            x
EVA 10mm                    x
```