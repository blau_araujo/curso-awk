# Aula 6 - Arrays multidimensionais

- [Vídeo desta aula](https://youtu.be/4sPLHOf7JOk)

## 6.1 - O que é uma array multidimensional

* Em uma array multidimensional, os elementos são identificados por uma sequência de índices.
* Cada linguagem implementa sua notação para representar a sequência de índices.
* No AWK, esta seria a representação de um elemento de uma array bidimensional:

```
array[dim1,dim2]
```

Mas, lembre-se: no AWK, todas as arrays são associativas! Portanto...

> No AWK, uma sequência de índices é, na verdade, um índice único gerado pela interpolação das strings que representam as dimensões da array unidas por um caractere separador padrão.

No AWK, o separador padrão é definido na variável interna `SUBSEP`, cujo valor padrão é o caractere não imprimível `\034 (Ctrl+\)`. Depois de feita a concatenação das strings dos índices com o separador, o AWK enxerga apenas um índice normal de uma array unidimensional. Na prática, as duas representações abaixo são equivalentes:

```
array[5,7]
array[5 SUBSEP 7]
```

## 6.2 - Como fica o operador 'in'

Não há nenhuma diferença no uso do operador. A diferença está apenas na representação da sequência de índices, que agora deve apresentar a lista de dimensões entre parêntesis:

```
if ( (dim1, dim2, ...) in array ) ...
```

## 6.3 - Percorrendo os elementos de uma array multidimensional

* O AWK não trabalha com arrays multidimensionais.
* O AWK pode trabalhar arrays "como se elas fossem" multidimensionais.
* Logo, a iteração dos elementos precisa ser trabalhada de uma forma que resulte nas strings combinadas dos índices ou na separação das strings em substrings que representem as dimensões individualmente.

#### Exemplo 1: percorrendo todos os elementos

```
BEGIN {

    PROCINFO["sorted_in"]="@ind_num_asc"

    arr[0,0] = "A"
    arr[0,1] = "B"
    arr[0,2] = "C"
    arr[1,0] = "D"
    arr[1,1] = "E"
    arr[1,2] = "F"
    arr[2,0] = "G"
    arr[2,1] = "H"
    arr[2,2] = "I"

    for (elemento in arr) {
        dim = gensub(SUBSEP, ",", "g", elemento)
        print "arr[" dim "] =>", arr[elemento]
    }
}
```

#### Exemplo 2: dividindo a string do índice em dimensões

```
BEGIN {

    PROCINFO["sorted_in"]="@ind_num_asc"

    arr[0,0] = "A"
    arr[0,1] = "B"
    arr[0,2] = "C"
    arr[1,0] = "D"
    arr[1,1] = "E"
    arr[1,2] = "F"
    arr[2,0] = "G"
    arr[2,1] = "H"
    arr[2,2] = "I"

    for (elemento in arr) {
        split(elemento, dim, SUBSEP)
        print "arr[" dim[1] "," dim[2] "] =>", arr[elemento]
    }
}
```

## 6.4 - Trabalhando com arrays "realmente" multidimensionais (gawk)

Uma verdadeira array multidimensional é uma array de arrays, e o GNU/AWK oferece essa funcionalidade. Neste caso, os índices das dimensões são representados entre seu próprio par de colchetes:

```
array[dim1][dim2]...
```

Aqui, `[dim2]` é um elemento da array `array[dim1]`.

#### Exemplo:

```
BEGIN {

    # Define como o AWK irá tratar a ordem de iteração da array...
    PROCINFO["sorted_in"]="@ind_str_asc"

    aluno["João"]["bim1"] = 8
    aluno["João"]["bim2"] = 9.2
    aluno["João"]["bim3"] = 8.7
    aluno["João"]["bim4"] = 9.5

    aluno["Maria"]["bim1"] = 9
    aluno["Maria"]["bim2"] = 8.2
    aluno["Maria"]["bim3"] = 7.8
    aluno["Maria"]["bim4"] = 9.2

    aluno["Pedro"]["bim1"] = 7.5
    aluno["Pedro"]["bim2"] = 8.5
    aluno["Pedro"]["bim3"] = 6.8
    aluno["Pedro"]["bim4"] = 7.3

    print "\nNotas da turma no primeiro bimestre"
    for (elemento in aluno) {
        print elemento, ":", aluno[elemento]["bim1"]
    }

    print "\nNotas da Maria"
    for (elemento in aluno["Maria"]) {
        print elemento, ":", aluno["Maria"][elemento]
    }

    print "\nUma grade completa..."

    for (elemento in aluno) {
        print "\nAluno:", elemento
        for (nota in aluno[elemento]) {
            print nota, ":", aluno[elemento][nota]
        }
    }
}

```
